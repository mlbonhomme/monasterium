lateinisch
Deutsch
Latein
Deutsch/ Latein
Latein; Deutsch
Französisch
Hebräisch
Lateinisch.
lat.
dt.
Greacă
Română
Slavonă
latein
deutsch
nemščina
latinščina
Niederdeutsch
italienisch
Spanisch
Latin
LAT
D
CZ
LAT, IT
F
čes. b)
čes. (překlad z latiny)
čes.
něm.
lat. d)
lat., něm. (insert)
lat. b)
něm. a)
čes.(překlad z latiny)
latinský
slovenský
nemecký
latinsky
nemecky
maďarsky
maďarský
lat
ger
latger
Tschechisch
română (grafie chirilică)
Altserbisch.
Latein; Unterschrift Altserbisch.
Altitalienisch.
lateinsch
Lat.
česky
německy
česky, latinsky
česky, německy
französisch
Latein und Deutsch
Altpikardisch
nemščina, latinščina
latinščina; POŠK.
(in tschechischer Sprache)
Latin.
Latin and Italian.
Italian (originally Serbian?).
Italian.
Serbian.
Italian (originally Serbian).
Text in Italian, signature in Serbian.
Német
S
M
Magyar
latinski
latinski i talijanski
talijanski i latinski
talijanski
latinski i talijanski jezik
njemački
Latină, maghiară
Latină
Latină/maghiară
Maghiară
latinščina / Latein
latinšcina
nemšcina
старословенски
česky (insert latinsky)
česky a latinsky
německy, česky
česky, pouze listina č. 3 latinsky
latinsky, česky
chorvatsky
česky(latinsky)
latinsky i česky
latinsky, německy
německy, inzert česky
italsky
německy (insert česky)
německy (insert latinsky)
latinsky (inserty německy)
latinsky, notářské ověření německé
latinsky (německy)
listiny 1.-3. latinsky, 4. česky
latinsky (insert česky)
německy (1. insert latinsky, v 2. insertu část česky)
německy, inserty latinsky, česky
česky /latinsky/
latinsky (něm.)
latinsky; inzerovaná listina česky
italsky, (latinsky)
latinsky, (italsky)
latinsky a italsky
pergamen
latinsky, (česky)
něměcky
česky, insert latinsky
latinsky, italsky
latinsky/česky
česky?
latinsky/český
francouzsky
latinsky, (2. insert italsky)
italsko-latinsky
německy, latinsky, česky
česky; část insertů latinsky; 4. insert německy
německy, insert latinsky
německy (latinské a české inserty)
česky; ověřovací doložka německy
nemški
slovenski
_ ostali jeziki
angleški
Slavic.
Greek.
tschechisch
Deutsch; Latein
Deutsch, Latein
Deutsch, mit Ausnahme der lateinischen Jahresangabe: Anno Domini millesimo quadringentesimo vicesimonono am Ende der Urkunde und dem Kanzleivermerk rechts darunter: d(ominus) dux p(er) se ipsum (gewöhnlich lautete die Formel: Ad mandatum domini ..).
Latein, deutsch
Das innere Hölltallehen im Navis
Deutsch mit Ausnahme der lateinischen Bestätigung des Notars Jakob Muttinger, dass die Abschrift mit dem Original wörtlich übereinstimmt.
Lateinische Einleitung des Notars Johannes, danach die Verleihung der Freiheitsrechte durch Herzog Heinrich und deren Bestätigung durch Herzog Albrecht in deutscher Sprache, der abschließende Teil wiederum lateinisch.
Deutsch, Beglaubigung des Notars Jakob Muttinger in Latein.
Deutsch mit Ausnahme des lateinischen Kanzleivermerks auf der Plika rechts unten: Ad mandatum domini Regis proprium.
Deutsch, nur die Beglaubigung durch Notar Muttinger in lateinischer Sprache.
Deutsch, nur Einleitungssatz am Spiegel des Vorderdeckels, Jahresangaben S. 1r und v, 2r sowie 15r jeweils oben, und die Endsumme der Einnahmen am Spiegel des Hinterdeckels lateinisch.
Deutsch mit Ausnahme des letzten Satzes, der auf Latein verfasst wurde.
Deutsch mit Ausnahme einer später vom Pfarrer in lateinischer Sprache angebrachten Randnotiz, dass der Richter die Entschuldigung nicht richtig widergab und der Beschuldigte den Pfarrer niemals aufgesucht hat, sondern in seiner Hartnäckigkeit verblieben ist.
Deusch
Deutsch, die Beglaubigung des Notars Jakob Muttinger in Latein.
Deutsch mit Ausnahme der lateinischen Jahresangabe: Anno Domini millesimo quadringentesimo vicesimonono am Ende der Urkunden-Abschrift und dem Kanzleivermerk rechts darunter: d(ominus) dux p(er) se ipsum im oberen Teil (gewöhnlich lautete die Formel: Ad mandatum domini ..).
Deutsch, Hinterdeckel aus einer älteren Handschrift lateinisch.
Deutsch mit Ausnahme der Schlussbemerkung: Ita est. Ego Petrus ut supra .. ""
Deutsch mit Ausnahme des lateinischen Kanzleivermerks auf der Plika rechts unten: Ad mandatum domini Imperatoris proprium.
russisch
schwedisch
Plattdeutsch
Cseh
němčina
čeština
česky, německy, latinsky
latinsky (Jan Lucemburský, Zikmund Lucemburský), německy (Václav IV.)
latina
čeština, němčina
latina, čeština
latina a čeština
německy, (latinsky)
čeština, latina, trans.
němčina, latina, čeština
němčina-čeština-latina
latina a němčina
latina, čeština, němčina
latina, němčina, čeština
bulharsky
německy, latinsky
Gallego, escritura gótica cursiva.
Lat. u. Deutsch.
nem.
nem.peč. dorz.
slov.
nem
pritlač. Poruš.
lat. - nem.
lat./nem.
nem,.
nem,
lat./lat.
nem. lat.
peč.pritlač.
nem.[dorz.]
nam.
nem.,lat.
nem.peč.pritlač.
Neustand
lat./ nem.
Latino
Latino;
lateinisch; deutsch
englisch
französchisch
deutsch; russchis
Deutsch/Latein
Olasz
G
singhalesisch / tamil
lateinisch/griechisch
griechisch
lateinisch / polnisch
mitteldeutsch
arabisch / persisch / türkisch
niederdeutsch
spanisch
englisch/lateinisch
lateinisch/spanisch
deutsch/französisch
italienisch/lateinisch
lat. und it.
deutsch (ripuarisch)
niederdeutsch; lateinisch
niederländisch
niederfränkisch
hochdeutsch
lat., dt.
dt., lat.
lat., ung.
Or.; Papier, Libell in Pergamentumschlag; Siegel fehlt.
Or.; Perg., leichter Mäusefraß; mit Siegel.
Insert lat.
Rahmenurkunde lat. Inserte teils deutsch teils lateinisch
Or.; Papier; aufgedr. Siegel abgefallen.
Rahmenurkunde und die ersten 4 Inserte lateinisch, die letzten drei Inserte deutsch
Or.; Perg.;: mit besch. Marktsiegel.
Or.; Papier, 6 Bl. gebunden, Wasserschaden, unbesiegelt; Chirograph, mit Unterschrift des Sekretärs Ulrich Vischer.
Or.; Papier, 4 Bl. gebunden; mit zwei Papiersiegeln.
lat, Aufzählung der Kleinodien in deutsch
lat./dt.
Or.; Papier, Doppelbl.; mit drei Papiersiegeln.
německy a latinsky
latinsky, česky, německy
latinsky a německy
česky a německy
německo
Latein/Deutsch
Lateinisch
Holländisch
russisch; lateinisch
ebräisch
deutsch; scwedisch
hebräisch; Lateinisch
russisch; deutsch
plattdeutsch
český
německý
latinský, německý
latinský, český
latinský, italský
latinsky a německy z konce 15. stol.
latinsky, český
latinsky /německý/
německy /zajímavý jazyk/
italsky, latinsky
(italský), latinsky
cesky
Latin, Serbian
German
Old Slavic
Hungarian
Greek, Italian
de.
hebräisch
darumbe so geben die Aussteller den Käufern und ihren Erben disen brief , . . versigilt mit ihres perchmaisters und perchherren insigiln, hern Gerungs des Choln und hern Nichlas in dem Perchhof, und mit hern Dietreichs insigil dez Vrbaetschen, zu den [58] zeiten mu/ensmaister ze Wienne, und mit hern Seyfrits insigil dez Minngangs, die diser sache gezeuge sint mit irn insigiln. ... Geben ze Wienne, nach Christes gepurt dreutzehen hundert jar, darnach in dem neununddreizgisten jar, dez naechsten suntages nach dem ostertage.
Deutsch (bis auf die Jahreszahl)
Lateinische Sprache
dt
Kerbzettel
dt. II: Ausf.; dt.
dt. (durch Einschnitte rechtsungültig
                            gemacht)
dt. (durch Einschnitt rechtsungültig
                            gemacht)
latino
deutsch, lateinisch
deutsch, lateinisches Transsumpt
deutsch und tschechisch
deutsch; tschechisch
deutsch / lateinisch
deutsch; lateinisch
Transumierte Urkunde latein; Beglaubigung in deutsch
lateinisch, deutsch
deutsch und latein
latinsky, německy, česky
Hochdeutsch
Ndt.
Hdt.
Latein/Ndt.
Mndt.
Nieder-/Hochdeutsch
Nieder-/Oberdeutsch
latein.
ital.
lat. (Bullatica)
lat. u. ital.
lat./ dt.
lst.
lat. u. dt.
lat. Perg.
lat. Pap.
dt. Perg.
let.
Dt.
dt./lat.
lat. / dt.
dt. u. lat.
dt-
dt.Perg.
n Braumeister Kantor
Brief
ger.
lat., it.
lt.
dt. u. lat
Latein (Littera)
Latein, Littera
Perg.
Pap.
dt,
lat.u. dt. (Insert)
deutsch?
deutsch/Latein
lateinisch und deutsch
j. łaciński
Klosterneuburg
Wien St. Dorothea
Germană
Rusă
Die Abschrift vom 18. Jh gibt den Text in der zeitgenössischen russischslawischen Sprache wieder. In den Editionen von Miklosich und Safarik (übernommen von Novakovic in Zakonski spomenici) wurden die Formen der altserbischen Sprache des 14. Jhs. angepasst.
dt. und lat.
handschriftl. Eintrag des Notars = Ortenburg-Archiv
a) lat. b) lat.
lat. und dt.
dt. = Ortenburg-Archiv
Italienisch
latinščina; tiskan formular
latinščina, nemščina
(des Originales:) Latein; (der Übersetzung:) Deutsch
Latein, Deutsch, Tschechisch
(des Briefes von 1762:) Deutsch
(des Originales:) Latein, (der Übersetzung:) Deutsch
Keine Angabe
wohl Deutsch und Latein (?)
Latein, Deutsch
Keine Angabe.
Deutsch (?)
Original: wohl Tschechisch; Deutsch
Deutsche Version
lateinische Version
Deutsche Fassung
deutsche Version
Aus einem Copialbuche des Klosters Baumgartenberg vom Jahre 1311.
Ungarisch
A1 Latein. - A2 Deutsch.
(A) Latein. - (B) Deutsch.
Gaming
Latein und Deutsch.
Latein+Deutsch
(der Abschrift:) Deutsch
Abschrift Pap. (17. Jh.).
jetzt fehlenden Siegeln).
. Latein
latinský, zdobený zlatými písmeny
německý, zdobený zlatým písmem
německý, latinský
český, německý
německý, zdobený
10 Pergamentblätter in Papierumschlag
Latein?
Siehe: Urkunde N° 90.
dt.;
lat. bzw dt. (Insert)
lat.; Chirograph
lat. + dt.
Deutsch [?]
