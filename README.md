# monasterium

* monasterium_overall_stats.csv : `charter, fonds/collection, number of image links, image links, has image links, languages, date range, has transcription, google ocr` for **all charters** in `mom-dump20190515`.  
* mycollection_infos.csv : `mycollection, nr of charters, image link(s) only, transcription only, image+transcription, no image no transcription, avg nr of images, language(s), date min, date max` for **all mycollection collections** (metadata.mycollection.public).  
* collections_infos.csv : `collection, nr of charters, image link(s) only, transcription only, image+transcription, no image no transcription, avg nr of images, language(s), date min, date max` for **all collections** (metadata.collection.public).
