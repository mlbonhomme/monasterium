xquery version "3.1";
declare namespace atom="http://www.w3.org/2005/Atom";
declare namespace cei="http://www.monasterium.net/NS/cei";
declare namespace xrx="http://www.monasterium.net/NS/xrx";
<charters
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:cei="http://www.monasterium.net/NS/cei"
xmlns:xrx="http://www.monasterium.net/NS/xrx"
>{
for $u in subsequence(collection('/db/mom-data/metadata.charter.public/')//atom:entry,1,10)
    let $tokens := tokenize($u/atom:id/string(),'/')
    let $collfondName := replace(concat($tokens[3],'/', $tokens[4]),concat('/',$tokens[last()]),'')
    let $coll := collection('/db/mom-data/metadata.collection.public/')//atom:id[contains(.,$collfondName)]
    let $fond := collection('/db/mom-data/metadata.fond.public/')//atom:id[contains(.,$collfondName)]
    let $base-url := if($coll!='') then (
        $coll/ancestor::atom:entry//cei:image_server_address/string()
    )
    else if ($fond!='') then (
        doc(replace($fond,/base-uri(), 'ead.xml','preferences.xml'))//xrx:param[@name="image-server-base-url"]/string()
    )
    else ('')
return
(:ID; Version of; Fonds|Collection; version-of ; Date ; Number of images ;  Images-Links ; Transcription-Available (yes/no) ; Language) :)
<charter id="{$u/atom:id/string()}">
    {<coll_fond>{$collfondName}</coll_fond>,
        $u/atom:link,
        $u//cei:issued/(cei:date, cei:dateRange),
        <images n="{count($u//cei:chDesc//cei:figure/cei:graphic[@url!=''])}" base-url="{
            if(starts-with($base-url,'http')) then ($base-url) else (concat('http://',$base-url))
            }">
            {
                for $image in $u//cei:chDesc//cei:figure/cei:graphic
                return $image
            }
        </images>,
        <textlength>{$u//cei:tenor/string-length(string())}</textlength>,
        $u//cei:langMOM
}
</charter>
}
</charters>