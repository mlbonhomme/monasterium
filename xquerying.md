basex
open monasterium

## tests

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; //cei:tenor  
: finds all cei:tenor in all the files  

xquery for $file in db:open('monasterium', '/metadata.charter.public/AbteiEberbach') return document-uri($file)  
xquery for $file in db:open('monasterium', '/metadata.charter.public/AT-AWMK') return document-uri($file)  

xquery for $file in collection('monasterium/metadata.charter.public/AbteiEberbach') return document-uri($file)  
xquery for $file in collection('monasterium/metadata.charter.public/AT-AWMK') return document-uri($file)  
xquery for $file in collection('monasterium/metadata.charter.public') return document-uri($file)  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-BAH') return (document-uri($file), count($file//cei:tenor))  
xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-BAH') return (document-uri($file), count($file//cei:graphic))  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-BAH') where count($file//cei:graphic) > 0 and count($file//cei:tenor) > 0 return (document-uri($file), count($file//cei:graphic), count($file//cei:tenor))  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-BAH') where some $tenor in $file//cei:tenor/text() satisfies (fn:matches($tenor, "\w+")) return document-uri($file)  

> some $child in $node/text() satisfies (fn:matches($child, "\w+"))

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-BAH') let $charter := $file//cei:text where some $tenor in $charter//cei:tenor/text() satisfies (fn:matches($tenor, "\w+")) return document-uri($file)   

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public/DE-StaASpeyer/1U') where $file//cei:graphic/@url[not(.="")] return document-uri($file)  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where some $person in $file//cei:tenor//cei:persName/text() satisfies (fn:matches($person, "\w+")) return document-uri($file)  

-------

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text)  
647593 charters total  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:graphic)  
887335 images total  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:tenor)  
83.583 tenor tags  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:abstract)  
557450 charters with abstract (557.450 asbtract tags)  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:lang_MOM)  
185674 charters with a language tag (185.674 language tags)  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where $file//cei:graphic/@url[not(.="")] return document-uri($file)  
472126 charters with non-empty image urls  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where some $tenor in $file//cei:tenor/text() satisfies (fn:matches($tenor, "\w+")) return document-uri($file)  
30605 charters with non-empty tenor tags  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where some $ptenor in $file//cei:pTenor/text() satisfies (fn:matches($ptenor, "\w+")) return document-uri($file)  
41992 charters with non-empty pTenor tags  

there are some files in common > 72.177 files with non empty tenor and/or ptenor tags.  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:persName)  
182742 persName tags in charters  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:placeName)  
432111 placeName tags in charters  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:geogName)  
37588 geogName tags in charters  

intersection of files with non-empty urls and files with non-empty transcriptions = 47.816 files  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; count(collection('monasterium/metadata.charter.public')//cei:text//cei:dateRange)  
231.760 dateRange tags  

## global stats

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where $file//cei:graphic/@url[not(.="")] let $image_urls := $file//cei:graphic/@url[not(.="")] let $tenor := $file//cei:tenor/text() return concat('"', document-uri($file),'"', ', ', count($image_urls), ', ','"', string-join($image_urls, ", "), '"', ', yes')  
=> get, for files with images urls, file_path, number of images, the urls, a yes to say there are image links  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where $file//cei:text//cei:dateRange/@from[not(.="")] and $file//cei:text//cei:dateRange/@to[not(.="")] return concat('"', document-uri($file),'"', ', ', string-join($file//cei:text//cei:dateRange/@\*, ', '))  
=> get, for files with valid dateRange : the date range  

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; for $file in collection('monasterium/metadata.charter.public') where some $language in $file//cei:lang_MOM/text() satisfies (fn:matches($language, "\w+")) return concat('"', document-uri($file),'"', ', ', '"', string-join($file//cei:lang_MOM/text(), ', '), '"')  
=> get, for files with a valid language, the language(s)  

## by collection / fonds

138 'Google data' / 'export aus google' / 'google' in metadata.collection.public (`grep -rinw 'Google data' . | wc -l`)  
D. Stutzmann : **205** collections de Google  
Redmine IRHT : 137 documents (BischoefeSpeyer en double)  
Mine : les mêmes + monasterium/metadata.collection.public/SchneiderTest

xquery declare namespace cei="http://www.monasterium.net/NS/cei"; declare namespace xrx="http://www.monasterium.net/NS/xrx"; for $file in collection('monasterium/metadata.fond.public/AT-DAGS') where $file//xrx:param[@name="image-server-base-url"]/text() satisfies (fn:matches($file//xrx:param[@name="image-server-base-url"]/text(), "\w+")) let $base-url := $file//xrx:param[@name="image-server-base-url"]/text() return concat('"', document-uri($file),'"', ', ', '"', $base-url, '"')

## on iccos

java -cp BaseX.jar org.basex.BaseX in `basex` folder
